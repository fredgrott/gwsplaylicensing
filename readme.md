GWSPlayLicensing
---

Android Project Library for Play Licensing.

# Implementatin Notes

APLs do not yet supoprt AIDLs(maybe android tools 21?), thus Google precompiled 
the aidls required and than posted the code generated for those aidls within the src 
of the play licensing extras in the SDK.

Copy of the original aidl files is in the aidl folder. My min target is se tto 11 if you need 
a lower min target fork this repo and change it for your use.

# License

Apache License 2.0

# Credits

Android Open Source Project